# -*- coding: utf-8 -*-

""" Distributed under GNU Affero GPL license.

    ContactMe v2.0 28/08/2019.

    Copyright (C) 2019  Davide Leone <leonedavide[at]protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#--- LIBRARIES IMPORT ---
from config import staff_users_file, owner_id, bot, telegram
from functools import wraps
from pathlib import Path

#--- NEEDED FILES CHECK ---
id_list = Path(staff_users_file)
if not id_list.is_file():
    #The staff_users_file was not created yet
    #The owner ID is added
    with open(staff_users_file,'w') as file:
        file.write(str(owner_id)+"\n")

#--- FUNCTIONS ---
def add_user_to_staff(user_id):
    #The user is added to the staff
    user_id = int(user_id)
    staff = get_staff()

    if (not is_staff(user_id)):
        staff.append(user_id)
        text = ''

        for user_id in staff:
            text += str(user_id)+"\n"

        with open(staff_users_file,'w') as file:
            file.write(text)
            
        return True

    return False

def admin_answer_to(update):
    #Given a message an admin has answered to, try to get the ID of the user the admin is trying to reply
    if (update.message.reply_to_message.forward_from == None) and (update.message.reply_to_message.entities == []):
        #The admin tried to answer a user who has disabled the link to his account for forwarded messages, and therefore is unrecognizable
        return False
                
    else:
        #It is possible to identify the user
        if (update.message.reply_to_message.forward_from == None):
            #This is the case where the user is identified by an entities in a bot message which contains the user's id
            reply_to_id = update.message.reply_to_message.entities[0].user.id
            return reply_to_id
        else:
            #This is the most common case
            reply_to_id = update.message.reply_to_message.forward_from.id
            return reply_to_id
            
def forward_message(update, except_admin=None):
    #A message is forwarded to all the staff. An admin may be exempted if needed (es. the message to be forwarded is his)
    forward_message = False
    staff = get_staff()
    
    if except_admin in staff:
        staff.remove(except_admin)

    for admin in staff:
        try:
            forward_message = update.message.forward(admin)
        except telegram.error.Unauthorized:
            #The bot was blocked by the user
            None
            
    return forward_message    

def get_staff():
    #This function reads the .txt file with all the ids
    #A list of int is returned
    with open(staff_users_file,'r') as file:
        staff = file.read()

    staff = staff.replace('\n\n','\n') #Avoid double empty line
    staff = staff.split()
    staff = [int(user_id) for user_id in staff]
    return staff


def is_staff(user_id):
    #Returns true if the user is in the staff, false if it's not
    user_id = int(user_id)
    staff = get_staff()
    return (user_id in staff)

def remove_user_from_staff(user_id):
    #The user is removed from the staff
    user_id = int(user_id)
    staff = get_staff()

    if is_staff(user_id) and user_id != int(owner_id): 
        
        staff.remove(user_id)
        text = ''

        for user_id in staff:
            text += str(user_id)+"\n"

        with open(staff_users_file,'w') as file:
            file.write(text)
            
        return True

    return False

def send_message(text,except_admin=None):
    #A message is sent to all the staff. An admin may be exempted if needed (es. the message is his)
    staff = get_staff()
    if except_admin in staff:
        staff.remove(except_admin)

    for admin in staff:
        try:
            bot.sendMessage(
                chat_id = admin,
                text = text,
                parse_mode = telegram.ParseMode.HTML,
                )
        except telegram.error.Unauthorized:
            #The bot was blocked by the user
            None
            

def staff_only_function(func):
    #A decorator to allow access to a handler only for staff members
    @wraps(func)
    
    def wrapped(update, context, *args, **kwargs):
        user_id = update.effective_user.id
        
        if user_id not in get_staff():
            return
        
        return func(update, context, *args, **kwargs)
    
    return wrapped

def staff_excluded_function(func):
    #A decorator to obstruct access to a handler for staff members
    @wraps(func)
    
    def wrapped(update, context, *args, **kwargs):
        user_id = update.effective_user.id
        
        if user_id in get_staff():
            return
        
        return func(update, context, *args, **kwargs)
    
    return wrapped
