# -*- coding: utf-8 -*-

""" Distributed under GNU Affero GPL license.

    ContactMe v2.0 28/08/2019.

    Copyright (C) 2019  Davide Leone <leonedavide[at]protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from config import *

def send_anything(chat_id, message):
    #The message is re-sent by the bot to the chat_id specified
    #Returns false if not successfully 
    caption = message.caption_markdown 

    if message.animation != None:

        bot.send_animation(
            chat_id = chat_id,
            animation = message.animation.file_id,
            )

        return True

    elif message.audio != None:

        bot.send_audio(
            chat_id = chat_id,
            audio = message.audio.file_id,
            performer = message.audio.performer,
            caption = caption,
            parse_mode = telegram.ParseMode.MARKDOWN
            )

        return True

    elif message.contact != None:

        bot.send_contact(
            chat_id = chat_id,
            phone_number = message.contact.phone_number,
            first_name = message.contact.first_name,
            last_name = message.contact.last_name
            )

        return True

    elif message.document != None:

        bot.send_document(
            chat_id = chat_id,
            document = message.document.file_id,
            caption = caption,
            parse_mode = telegram.ParseMode.MARKDOWN
            )

        return True
        
    elif message.location != None:

        bot.send_location(
            chat_id = chat_id,
            latitude = message.location.latitude,
            longitude = message.location.latitude
            )

        return True

    elif message.photo != []:

        bot.send_photo(
            chat_id = chat_id,
            photo = message.photo[-1]['file_id'],
            caption = caption,
            parse_mode = telegram.ParseMode.MARKDOWN
            )

        return True

    elif message.sticker != None:

        bot.send_sticker(
            chat_id = chat_id,
            sticker = message.sticker.file_id
            )

        return True

    elif message.text != None:

        bot.send_message(
            chat_id = chat_id,
            text = message.text_markdown,
            parse_mode = telegram.ParseMode.MARKDOWN)

        return True
    
    elif message.venue != None:
        
        bot.send_venue(
            chat_id = chat_id,
            latitude = message.venue.location.latitude,
            longitude = message.venue.location.longitude,
            title = message.venue.title,
            address = message.venue.address,
            foursquare_id = message.venue.foursquare_id,
            foursquare_type = message.venue.foursquare_type
            )
        
        return True
    
    elif message.video != None:

        bot.send_video(
            chat_id = chat_id,
            video = message.video.file_id,
            duration = message.video.duration,
            caption = caption,
            parse_mode = telegram.ParseMode.MARKDOWN
            )

        return True
        
    elif message.video_note != None:

        bot.send_video_note(
            chat_id = chat_id,
            video_note = message.video_note.file_id
            )

        return True
        
    elif message.voice != None:

        bot.send_voice(
            chat_id = chat_id,
            voice = message.voice.file_id
            )

        return True

    else:
        return False
